/*@****************************************************************************
*
*   █████╗ ███╗   ███╗ ██████╗ ███████╗    ██████╗
*  ██╔══██╗████╗ ████║██╔═══██╗██╔════╝    ╚════██╗
*  ███████║██╔████╔██║██║   ██║███████╗     █████╔╝
*  ██╔══██║██║╚██╔╝██║██║   ██║╚════██║    ██╔═══╝
*  ██║  ██║██║ ╚═╝ ██║╚██████╔╝███████║    ███████╗
*  ╚═╝  ╚═╝╚═╝     ╚═╝ ╚═════╝ ╚══════╝    ╚══════╝
*
****************************************************************************@*/
//
// AVM-32.
// Olivier Bori
// Version 0.1
// 10/08/2019
// (c) THE PATREON AMOS GROUPE
//
// Compiled with AMOS 2 compiler version 0.7 - 07/08/2019 on the 16/08/2019-15:07:38
//

function Application( canvasId )
{
	this.manifest = JSON.parse('{"version":"6","infos":{"applicationName":"AVM-32.","author":"Olivier Bori","version":"Version 0.1","date":"10/08/2019","copyright":"(c) THE PATREON AMOS GROUPE","start":"kernal.amos"},"compilation":{"speed":"fast","syntax":"amosPro","emulation":"1200","usePalette":true,"useShortColors":true,"showCopperBlack":false,"unlimitedScreens":true,"unlimitedWindows":true,"maskHardwareCoordinates":false,"endian":"big"},"display":{"tvStandard":"pal","width":1024,"height":768,"background":"color","backgroundColor":"#000000","bodyBackgroundColor":"#000000","bodyBackgroundImage":"./runtime/resources/star_night.jpeg","scaleX":2,"scaleY":2,"screenScale":4,"fps":false,"fpsFont":"12px Verdana","fpsColor":"#FFFF00","fpsX":10,"fpsY":16,"fullPage":1,"fullScreen":1,"keepProportions":true,"fullScreenIcon":true,"fullScreenIconX":-34,"fullScreenIconY":2,"fullScreenIconImage":"./runtime/resources/full_screen.png","smallScreenIconImage":"./runtime/resources/small_screen.png"},"sprites":{"collisionBoxed":false,"collisionPrecision":1,"collisionAlphaThreshold":1},"fonts":{"listFonts":"amiga","amiga":["times"],"google":[]},"default":{"screen":{"x":120,"y":42,"width":320,"height":256,"numberOfColors":32,"pixelMode":"lowres","palette":["#000000","#AA4400","#FFFFFF","#000000","#FF0000","#00FF00","#0000FF","#666666","#555555","#333333","#773333","#337733","#777733","#333377","#773377","#337777","#000000","#EECC88","#CC6600","#EEAA00","#2277FF","#4499DD","#55AAEE","#AADDFF","#BBDDFF","#CCEEFF","#FFFFFF","#440088","#AA00EE","#EE00EE","#EE0088","#EEEEEE"],"window":{"x":0,"y":0,"fontWidth":8,"fontHeight":8,"border":0,"paper":1,"pen":2,"background":"opaque","font":{"name":"topaz","type":"amiga","height":8},"cursorImage":"./runtime/resources/cursor_pc.png","cursorColors":[{"r":68,"g":68,"b":0,"a":128},{"r":136,"g":136,"b":0,"a":128},{"r":187,"g":187,"b":0,"a":128},{"r":221,"g":221,"b":0,"a":128},{"r":238,"g":238,"b":0,"a":128},{"r":255,"g":255,"b":34,"a":128},{"r":255,"g":255,"b":136,"a":128},{"r":255,"g":255,"b":204,"a":128},{"r":255,"g":255,"b":255,"a":128},{"r":170,"g":170,"b":255,"a":128},{"r":136,"g":136,"b":204,"a":128},{"r":102,"g":102,"b":170,"a":128},{"r":34,"g":34,"b":102,"a":128},{"r":0,"g":0,"b":68,"a":128},{"r":0,"g":0,"b":17,"a":128},{"r":0,"g":0,"b":0,"a":128}]}}}}');
	this.parent = this;
	this.root = this;
	this.amos=new AMOS(canvasId,this.manifest);

	// Compiled program begins here
	// ----------------------------
	this.vars=
	{
		DISPLAY_MOD:0,
		DEFAULT_COLOR:0,
		PAPER_COLOR:0,
		PEN_COLOR:0,
		X$:"",
		L1:0,
		L2:0,
		L3:0,
		L4:0,
		L5:0,
		L6:0,
		L7:0,
		L8:0
	}
	this.blocks=[];
	this.blocks[0]=function()
	{
		// ' AVM-65 AMOS VIRTUAL MACHINE
		// ' KERNAL V0.1B BY OLIVIER BORI
		// ' FIRST COMMIT 10/08/2019
	};
	this.blocks[1]=function()
	{
		// _CONFIG:
		// DISPLAY_MOD   = 40   :
		this.amos.sourcePos="5:4";
		this.vars.DISPLAY_MOD=40;
		// DEFAULT_COLOR = 1    :
		this.amos.sourcePos="6:4";
		this.vars.DEFAULT_COLOR=1;
		// PAPER_COLOR   = 1    :
		this.amos.sourcePos="7:4";
		this.vars.PAPER_COLOR=1;
		// PEN_COLOR     = 2    :
		this.amos.sourcePos="8:4";
		this.vars.PEN_COLOR=2;
	};
	this.blocks[2]=function()
	{
		// _INIT:
		// If DISPLAY_MOD > 40 Then DISPLAY_MOD_80
		this.amos.sourcePos="13:4";
		if(!(this.vars.DISPLAY_MOD>40))
			return{type:1,label:4};
		this.amos.sourcePos="13:29";
		return {type:4,procedure:procDISPLAY_MOD_80,args:{}};
	};
	this.blocks[3]=function()
	{
	};
	this.blocks[4]=function()
	{
		// AVM_65_POWER_ON
		this.amos.sourcePos="14:4";
		return {type:4,procedure:procAVM_65_POWER_ON,args:{}};
	};
	this.blocks[5]=function()
	{
	};
	this.blocks[6]=function()
	{
		// _LOOP:
		// Do
		this.amos.sourcePos="17:4";
	};
	this.blocks[7]=function()
	{
		// X$=Inkey$ : if X$ <> "" Then print X$;
		this.amos.sourcePos="18:8";
		this.vars.X$=this.amos.inkey$();
		this.amos.sourcePos="18:20";
		if(!(this.vars.X$!=""))
			return{type:1,label:8};
		this.amos.sourcePos="18:37";
		this.amos.currentScreen.currentWindow.print(this.vars.X$,false);
	};
	this.blocks[8]=function()
	{
		// Loop
		this.amos.sourcePos="19:4";
		return{type:1,label:7};
	};
	this.blocks[9]=function()
	{
		// End
		this.amos.sourcePos="20:0";
		return{type:0}
		// ' PROCEDURE AREA
		// ' --------------
		// ' TEST CODE AREA
		// ' --------------
	};
	this.blocks[10]=function()
	{
		// _SET_CURS:
		// L1=%11111111 : L2=%11111111 : L3=%11111111 : L4=%11111111 : L5=%11111111 : L6=%11111111 : L7=%11111111 : L8=%11111111
		this.amos.sourcePos="62:4";
		this.vars.L1=0b11111111;
		this.amos.sourcePos="62:19";
		this.vars.L2=0b11111111;
		this.amos.sourcePos="62:34";
		this.vars.L3=0b11111111;
		this.amos.sourcePos="62:49";
		this.vars.L4=0b11111111;
		this.amos.sourcePos="62:64";
		this.vars.L5=0b11111111;
		this.amos.sourcePos="62:79";
		this.vars.L6=0b11111111;
		this.amos.sourcePos="62:94";
		this.vars.L7=0b11111111;
		this.amos.sourcePos="62:109";
		this.vars.L8=0b11111111;
		// Set Curs L1,L2,L3,L4,L5,L6,L7,L8
		this.amos.sourcePos="63:4";
		this.amos.currentScreen.getCurrentWindow().setCurs(this.vars.L1,this.vars.L2,this.vars.L3,this.vars.L4,this.vars.L5,this.vars.L6,this.vars.L7,this.vars.L8);
		// Return
		this.amos.sourcePos="64:4";
		return{type:3};
		// End
		this.amos.sourcePos="65:0";
		return{type:0}
	};
	this.blocks[11]=function()
	{
		// _CODE_TEST:
		// Get Fonts : Set Font 2 : Text 100,100,"AMOS"
		this.amos.sourcePos="68:4";
		this.amos.fonts.getFonts();
		this.amos.sourcePos="68:16";
		return{type:8,instruction:"setFont",args:[2]};
	};
	this.blocks[12]=function()
	{
		this.amos.sourcePos="68:29";
		this.amos.currentScreen.text(100,100,"AMOS");
		// Return
		this.amos.sourcePos="69:4";
		return{type:3};
		// End
		this.amos.sourcePos="70:0";
		return{type:0}
	};
	this.blocks[13]=function()
	{
		return {type:0};
	};

	// Labels...
	this.labels=
	{
		_CONFIG:
		{
			dataPosition:0,
			labelBlock:1
		},
		_INIT:
		{
			dataPosition:0,
			labelBlock:2
		},
		_LOOP:
		{
			dataPosition:0,
			labelBlock:6
		},
		_SET_CURS:
		{
			dataPosition:0,
			labelBlock:10
		},
		_CODE_TEST:
		{
			dataPosition:0,
			labelBlock:11
		}
	};
	this.amos.run(this,0,null);
};
function procAVM_65_POWER_ON(amos,root)
{
	this.amos=amos;
	this.root=root;
	var self=this;
	this.reset=function()
	{
		self.vars=
		{

		};
	};
	this.blocks=[];
	this.blocks[0]=function()
	{
		// Cls DEFAULT_COLOR : Paper PAPER_COLOR : Pen PEN_COLOR : Curs On  ' Config Screen
		this.amos.sourcePos="25:4";
		this.amos.currentScreen.cls(this.root.vars.DEFAULT_COLOR);
		this.amos.sourcePos="25:24";
		this.amos.currentScreen.getCurrentWindow().setPaper(this.root.vars.PAPER_COLOR);
		this.amos.sourcePos="25:44";
		this.amos.currentScreen.getCurrentWindow().setPen(this.root.vars.PEN_COLOR);
		this.amos.sourcePos="25:60";
		this.amos.currentScreen.getCurrentWindow().setCursor(true);
		// Locate 4,1 : PRINT_CENTER["**** AVM-65 AMOS BASIC V0.7 ****"  ,1]
		this.amos.sourcePos="27:4";
		this.amos.currentScreen.getCurrentWindow().locate(4,1);
		this.amos.sourcePos="27:17";
		return {type:4,procedure:procPRINT_CENTER,args:{TXT$:"**** AVM-65 AMOS BASIC V0.7 ****",UP:1}};
	};
	this.blocks[1]=function()
	{
		// Locate 3,3 : PRINT_CENTER["(C) 2019 - THE PATREON AMOS GROUPE",1]
		this.amos.sourcePos="28:4";
		this.amos.currentScreen.getCurrentWindow().locate(3,3);
		this.amos.sourcePos="28:17";
		return {type:4,procedure:procPRINT_CENTER,args:{TXT$:"(C) 2019 - THE PATREON AMOS GROUPE",UP:1}};
	};
	this.blocks[2]=function()
	{
		// Locate 0,6 : Print "READY."
		this.amos.sourcePos="29:4";
		this.amos.currentScreen.getCurrentWindow().locate(0,6);
		this.amos.sourcePos="29:17";
		this.amos.currentScreen.currentWindow.print("READY.",true);
		// End Proc
		return{type:0};
	};
};
function procDISPLAY_MOD_80(amos,root)
{
	this.amos=amos;
	this.root=root;
	var self=this;
	this.reset=function()
	{
		self.vars=
		{

		};
	};
	this.blocks=[];
	this.blocks[0]=function()
	{
		// Screen Open 1,640,200,16,Hires
		this.amos.sourcePos="33:4";
		this.amos.screenOpen(1,640,200,16,1);
		// End Proc
		return{type:0};
	};
};
function procPRINT_CENTER(amos,root)
{
	this.amos=amos;
	this.root=root;
	var self=this;
	this.reset=function()
	{
		self.vars=
		{
			TXT$:"",
			UP:0,
			SIZE_OF_STRING:0
		};
	};
	this.blocks=[];
	this.blocks[0]=function()
	{
		// SIZE_OF_STRING = Len(TXT$)
		this.amos.sourcePos="37:1";
		this.vars.SIZE_OF_STRING=Math.floor(this.vars.TXT$.length);
		// IF UP > 0 Then TXT$ = Upper$(TXT$)
		this.amos.sourcePos="38:4";
		if(!(this.vars.UP>0))
			return{type:1,label:1};
		this.amos.sourcePos="38:19";
		this.vars.TXT$=this.vars.TXT$.toUpperCase();
	};
	this.blocks[1]=function()
	{
		// Locate (40-SIZE_OF_STRING)/2, : Print TXT$  '// OUTPUT TEXT
		this.amos.sourcePos="39:4";
		this.amos.currentScreen.getCurrentWindow().locate((40-this.vars.SIZE_OF_STRING)/2,undefined);
		this.amos.sourcePos="39:36";
		this.amos.currentScreen.currentWindow.print(this.vars.TXT$,true);
		// End Proc
		return{type:0};
	};
};
function procPRINT_ASCII_LIST(amos,root)
{
	this.amos=amos;
	this.root=root;
	var self=this;
	this.reset=function()
	{
		self.vars=
		{

		};
	};
	this.blocks=[];
	this.blocks[0]=function()
	{
		// For I = 32 to 255
		this.amos.sourcePos="43:4";
		this.root.vars.I=32;
		if(this.root.vars.I>255)
			return{type:1,label:3};
	};
	this.blocks[1]=function()
	{
		// Print I,"=",chr$(I)
		this.amos.sourcePos="44:8";
		this.amos.currentScreen.currentWindow.print(this.amos.str$(this.root.vars.I)+"\t"+"="+"\t"+String.fromCharCode(this.root.vars.I),true);
		// wait 5
		this.amos.sourcePos="45:8";
		return{type:8,instruction:"wait",args:[5]};
	};
	this.blocks[2]=function()
	{
		// Next I
		this.amos.sourcePos="46:4";
		this.root.vars.I+=1;
		if(this.root.vars.I<=255)
			return{type:1,label:1}
	};
	this.blocks[3]=function()
	{
		// End Proc
		return{type:0};
	};
};
function procBEEP(amos,root)
{
	this.amos=amos;
	this.root=root;
	var self=this;
	this.reset=function()
	{
		self.vars=
		{

		};
	};
	this.blocks=[];
	this.blocks[0]=function()
	{
		// Javascript
	    //var mySound;
	    //mySound = new sound("bounce.mp3");
	    //mySound.play();
		// Javascript end
		// End Proc
		return{type:0};
	};
};
