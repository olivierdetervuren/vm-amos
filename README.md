# AVM-65 (Amos Virtual Machine)

AVM is a fantasy console based on Amos Basic by François Lionet.

Written as an educational project for myself to respond to a childhood dream I had 30 years ago, to create my own 8-bit computer.


## License
Writed by Olivier Bori

[CC0 1.0 (Public Domain)](LICENSE.md)
