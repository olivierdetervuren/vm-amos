/*@****************************************************************************
*
*   █████╗ ███╗   ███╗ ██████╗ ███████╗    ██████╗
*  ██╔══██╗████╗ ████║██╔═══██╗██╔════╝    ╚════██╗
*  ███████║██╔████╔██║██║   ██║███████╗     █████╔╝
*  ██╔══██║██║╚██╔╝██║██║   ██║╚════██║    ██╔═══╝
*  ██║  ██║██║ ╚═╝ ██║╚██████╔╝███████║    ███████╗
*  ╚═╝  ╚═╝╚═╝     ╚═╝ ╚═════╝ ╚══════╝    ╚══════╝
*
****************************************************************************@*/
//
// AVM-32.
// Olivier Bori
// Version 0.1
// 10/08/2019
// (c) THE PATREON AMOS GROUPE
//
// Compiled with AMOS 2 compiler version 0.7 - 07/08/2019 on the 16/08/2019-15:10:18
//

function Application( canvasId )
{
	this.manifest = JSON.parse('{"version":"6","infos":{"applicationName":"AVM-32.","author":"Olivier Bori","version":"Version 0.1","date":"10/08/2019","copyright":"(c) THE PATREON AMOS GROUPE","start":"editor.amos"},"compilation":{"speed":"fast","syntax":"amosPro","emulation":"1200","usePalette":true,"useShortColors":true,"showCopperBlack":false,"unlimitedScreens":true,"unlimitedWindows":true,"maskHardwareCoordinates":false,"endian":"big"},"display":{"tvStandard":"pal","width":1024,"height":768,"background":"color","backgroundColor":"#000000","bodyBackgroundColor":"#000000","bodyBackgroundImage":"./runtime/resources/star_night.jpeg","scaleX":2,"scaleY":2,"screenScale":4,"fps":false,"fpsFont":"12px Verdana","fpsColor":"#FFFF00","fpsX":10,"fpsY":16,"fullPage":1,"fullScreen":1,"keepProportions":true,"fullScreenIcon":true,"fullScreenIconX":-34,"fullScreenIconY":2,"fullScreenIconImage":"./runtime/resources/full_screen.png","smallScreenIconImage":"./runtime/resources/small_screen.png"},"sprites":{"collisionBoxed":false,"collisionPrecision":1,"collisionAlphaThreshold":1},"fonts":{"listFonts":"amiga","amiga":["times"],"google":[]},"default":{"screen":{"x":120,"y":42,"width":320,"height":256,"numberOfColors":32,"pixelMode":"lowres","palette":["#000000","#AA4400","#FFFFFF","#000000","#FF0000","#00FF00","#0000FF","#666666","#555555","#333333","#773333","#337733","#777733","#333377","#773377","#337777","#000000","#EECC88","#CC6600","#EEAA00","#2277FF","#4499DD","#55AAEE","#AADDFF","#BBDDFF","#CCEEFF","#FFFFFF","#440088","#AA00EE","#EE00EE","#EE0088","#EEEEEE"],"window":{"x":0,"y":0,"fontWidth":8,"fontHeight":8,"border":0,"paper":1,"pen":2,"background":"opaque","font":{"name":"topaz","type":"amiga","height":8},"cursorImage":"./runtime/resources/cursor_pc.png","cursorColors":[{"r":68,"g":68,"b":0,"a":128},{"r":136,"g":136,"b":0,"a":128},{"r":187,"g":187,"b":0,"a":128},{"r":221,"g":221,"b":0,"a":128},{"r":238,"g":238,"b":0,"a":128},{"r":255,"g":255,"b":34,"a":128},{"r":255,"g":255,"b":136,"a":128},{"r":255,"g":255,"b":204,"a":128},{"r":255,"g":255,"b":255,"a":128},{"r":170,"g":170,"b":255,"a":128},{"r":136,"g":136,"b":204,"a":128},{"r":102,"g":102,"b":170,"a":128},{"r":34,"g":34,"b":102,"a":128},{"r":0,"g":0,"b":68,"a":128},{"r":0,"g":0,"b":17,"a":128},{"r":0,"g":0,"b":0,"a":128}]}}}}');
	this.parent = this;
	this.root = this;
	this.amos=new AMOS(canvasId,this.manifest);

	// Compiled program begins here
	// ----------------------------
	this.blocks=[];
	this.blocks[0]=function()
	{
	};
	this.blocks[1]=function()
	{
		// 10 Print "Super çà marche"
		this.amos.sourcePos="0:3";
		this.amos.currentScreen.currentWindow.print("Super çà marche",true);
	};
	this.blocks[2]=function()
	{
		return {type:0};
	};

	// Labels...
	this.labels=
	{
		10:
		{
			dataPosition:0,
			labelBlock:1
		}
	};
	this.amos.run(this,0,null);
};
